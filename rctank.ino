#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <math.h>

#define BLYNK_PRINT Serial

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "YourAuthToken";

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "YourWLANSSID";
char pass[] = "YourWLANPassword";

// Store the x and y coordinates of your joystick
int x = 0;
int y = 0;

// Executed if blynks virtual pin V0 is activated
BLYNK_WRITE(V0) {
  // Should be values between -100 to 100
  x = param[0].asInt();
  y = param[1].asInt();

  // Prints X and Y values for debugging
  Serial.print("X: ");
  Serial.print(x);
  Serial.print(" Y: ");
  Serial.println(y);

  // Checks the x and y for unexpected values
  if (!verify(x, y)) {  
    Serial.println("Error. Overflow");
    x = 0;
    y = 0;
  }
}

void setup()
{
  Serial.begin(115200);

  // Connectes to the WLAN and to blynk
  Blynk.begin(auth, ssid, pass);

  // Setup the pins for the 2 motors
  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
}

void loop()
{
  Blynk.run();
  pwm();
}

// performs a pwm on the motors
void pwm() {
  int powerRight = 0;
  int powerLeft = 0;

  // Calculates how much power is needed for the motors
  // The power equals the length of the vector from P(0|0) to Q(x|y)
  // except when x into the opposide direction than the motor
  // in this case the power equals the value of y
  if (y >= 0) {
    if (x >= 0) {
      powerRight = vectorLength(x, y);
      powerLeft = y;
    } else {
      powerRight = y;
      powerLeft = vectorLength(x, y);
    }
  }

  // Performs a pwm for the left and right motor
  for (int i = 0; i < 100; i++) {
    if (i < powerRight) {
      digitalWrite(D0, HIGH);
    } else {
      digitalWrite(D0, LOW);
    }

    if (i < powerLeft) {
      digitalWrite(D1, HIGH);
    } else {
      digitalWrite(D1, LOW);
    }
  }
}

// Calculates the length between O and Q(x|y)
int vectorLength(int x, int y) {
  int len = (int)sqrt(pow(x, 2) + pow(y, 2));
  if (len > 100)
    len = 100;
  return len;
}

// Verifys, that x and y are in the expected range
bool verify(int x, int y) {
  return abs(x) <= 100 && abs(y) <= 100;
}
